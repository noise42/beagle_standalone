package mvntest;
import static spark.Spark.*;


import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.util.Base64;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import org.apache.log4j.BasicConfigurator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.awt.image.BufferedImage;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.ProcessBuilder.Redirect;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
public class App {
	
	public static String encodeToString(BufferedImage image, String type) {
        String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
 
        try {
            ImageIO.write(image, type, bos);
            byte[] imageBytes = bos.toByteArray();
 
            BASE64Encoder encoder = new BASE64Encoder();
            imageString = encoder.encode(imageBytes);
 
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageString;
    }

	public static BufferedImage decodeToImage(String imageString) {
		 
        BufferedImage image = null;
        byte[] imageByte;
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }
	
	final static Logger logger = Logger.getAnonymousLogger();

	private static void varna() throws IOException{
		//prova lancio varna - anche se non da errori non da output
		//update 1: con processbuilder si vedono gli errori: non trova le classi
		//update 2: dentro a .classpath si vede che Eclipse ha i jar a partire da src
		// 	quindi src/main/resources/lib/VARNAv3-93-API.jar
		//update 3: così vede pure fr.orsay!!!
		//con il path completo funziona sia pbuilder che exec
		//Standalone mac? -> sposta la libreria jar 
		
		//String cmd="java -cp VARNAv3-93-API.jar fr.orsay.lri.varna.applications.VARNAcmd -structureDBN '(((((.....))))..)' -o porcaccio.png";
		
		String[] cmdarray = {"java", 			 
				//problema work directory di processbuilder
			//	"-cp", "src/main/resources/lib/VARNAv3-93-API.jar", //questo funziona finché si è in eclipse.
				//"-cp", "hw_pack_lib/VARNAv3-93-API.jar", //questo funziona una volta esportato con subfolder accanto (hw_pack = nome pacchetto)
				"-cp", "include/VARNAv3-93.jar", //prova workaround.

				 "fr.orsay.lri.varna.applications.VARNAcmd",			                   
		                    "-structureDBN", "(((((.....))))..)",
		                    "-o", "porcaccio.png"
		};
		
		Runtime.getRuntime().exec(cmdarray);
		ProcessBuilder pb = new ProcessBuilder(cmdarray);
//		String[] cmdtmp = {"ls hw_pack.jar"};
//		ProcessBuilder pb = new ProcessBuilder(cmdtmp);

		//pb.redirectOutput(Redirect.INHERIT);
		//pb.redirectError(Redirect.INHERIT); //non trova fr.orsay...!
		pb.redirectError(Redirect.to(new File("varna.err.txt")));
		pb.redirectOutput(Redirect.to(new File("varna.out.txt")));
		
		Process q = pb.start();
		//pb.directory();
		System.out.println((System.getProperty("user.dir")));
	}
	
	private static void beagle(String path1, String path2) throws IOException{
		//prova lancio varna - anche se non da errori non da output
		//update 1: con processbuilder si vedono gli errori: non trova le classi
		//update 2: dentro a .classpath si vede che Eclipse ha i jar a partire da src
		// 	quindi src/main/resources/lib/VARNAv3-93-API.jar
		//update 3: così vede pure fr.orsay!!!
		//con il path completo funziona sia pbuilder che exec
		//Standalone mac? -> sposta la libreria jar 
		
		//String cmd="java -cp VARNAv3-93-API.jar fr.orsay.lri.varna.applications.VARNAcmd -structureDBN '(((((.....))))..)' -o porcaccio.png";
		
		String[] cmdarray = {"java", 			 
				//problema work directory di processbuilder

				"-jar", "include/AMBeR.jar", //prova workaround.                   
		                    "-input1", path1,
		                    "-input2", path2
		};
		
		Runtime.getRuntime().exec(cmdarray);
		ProcessBuilder pb = new ProcessBuilder(cmdarray);
//		String[] cmdtmp = {"ls hw_pack.jar"};
//		ProcessBuilder pb = new ProcessBuilder(cmdtmp);

		//pb.redirectOutput(Redirect.INHERIT);
		//pb.redirectError(Redirect.INHERIT); //non trova fr.orsay...!
		pb.redirectError(Redirect.to(new File("beagle.err.txt")));
		pb.redirectOutput(Redirect.to(new File("beagle.out.txt")));
		
		Process q = pb.start();
		//pb.directory();
		System.out.println((System.getProperty("user.dir")));
	}

	/**
	 * @param args
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) throws IOException, URISyntaxException {
		staticFileLocation("/public");	
		//String path = App.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		
		BasicConfigurator.configure();
		logger.info("Hello Beagle!");

		VelocityTemplateEngine ve = new VelocityTemplateEngine();
		String layout="templates/layout.vtl";
        
		
		get("/", (req, res) -> {
			HashMap model = new HashMap();
			model.put("template", "templates/hello.vtl");
			model.put("forna", "templates/forna_test.vtl");
			return new ModelAndView(model,layout);
		}, ve);
		
		get("/results", (req, res) -> {
			HashMap model = new HashMap();
			
			model.put("forna", "templates/results.vtl");
			System.out.println("[GET]results");
			return new ModelAndView(model,layout);
		}, ve);
		
		
		
		post("/save", (req, res) -> {
			// Note preferred way of declaring an array variable
			String data=req.body();
			data=URLDecoder.decode(data, "UTF-8");
			String name = data.split("=")[1].split("&image")[0];
			String base64Image = data.split("=")[2].split(",")[1];
/*
			System.out.println(name);
			System.out.println(base64Image);
*/
			BufferedImage image = null;
			byte[] imageByte = Base64.getDecoder().decode(base64Image);

			ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
			image = ImageIO.read(bis);
			bis.close();

			// write the image to a file
			File dir = new File("results");
			if (!dir.exists()){
				new File("results").mkdirs();
			}

			try{
				File outputfile = new File("results/"+name.replaceAll("/", "_")+".png");
				ImageIO.write(image, "png", outputfile);
			}catch(Exception e){
				e.printStackTrace();
			}

			HashMap model = new HashMap();
			model.put("template", "templates/hello.vtl");
			model.put("forna", "templates/forna_test.vtl");
			return new ModelAndView(model,layout);
		});


//		if(Desktop.isDesktopSupported())
//		{
//			Desktop.getDesktop().browse(new URI("http://localhost:4567"));
//		}

		get("/hello/:neim", (req, res) -> {
			
			//varna();
			return "Hello " + req.params(":neim");
		});

		

	}
}
